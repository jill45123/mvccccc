using Jose;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using WebApplication1.Security;

namespace WebApplication1
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            //WebAPI會判斷瀏覽器送出的Accept標頭來自動決定回應的格式  將xml支援格式設定移除
            GlobalConfiguration.Configuration.Formatters.XmlFormatter.SupportedMediaTypes.Clear();

            var contractResolver = (DefaultContractResolver)GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings.ContractResolver;
            contractResolver.IgnoreSerializableAttribute = true;
        }

        protected void Application_OnPostAuthenticateRequest(object sender, EventArgs e)
        {
            HttpRequest httpRequest = HttpContext.Current.Request;
            //string cookieName = WebConfigurationManager.AppSettings["CookieName"].ToString();
            string SecretKey = WebConfigurationManager.AppSettings["SecretKey"].ToString();
            var token = httpRequest.Headers["Authorization"];
            if (token != null)
            {
                JwtObject jwtObject = JWT.Decode<JwtObject>(Convert.ToString(token), Encoding.UTF8.GetBytes(SecretKey), JwsAlgorithm.HS512);

                if (Convert.ToDateTime(jwtObject.Expire) < DateTime.Now)
                {
                    throw new Exception("token 不存在");
                }

                string[] roles = jwtObject.Role.Split(new char[] { ',' });
                GenericIdentity identity = new GenericIdentity(jwtObject.Member_Id);
                GenericPrincipal principal = new GenericPrincipal(identity, roles);
                HttpContext.Current.User = principal;
                Thread.CurrentPrincipal = HttpContext.Current.User;


                


                /*
                Claim[] claims = new Claim[]
                {
                    new Claim(ClaimTypes.Name, jwtObject.Account),
                    new Claim(ClaimTypes.NameIdentifier, jwtObject.Account)
                };

                var claimsIdentity = new ClaimsIdentity(claims, cookieName);
                claimsIdentity.AddClaim(new Claim(@"http://schemas.microsoft.com/accesscontrolservice/2010/07/claims/identityprovider",
                                        "My Identity", @"http://www.w3.org/2001/XMLSchema#string"));

                HttpContext.Current.User = new GenericPrincipal(claimsIdentity, roles);
                Thread.CurrentPrincipal = HttpContext.Current.User;
                */
            }


        }
    }
}
