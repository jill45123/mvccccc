﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using WebApplication1.Models;
using WebApplication1.ViewModels;

namespace WebApplication1.Service
{
    public class ReviewsDBService
    {
        private readonly static string cnstr = ConfigurationManager.ConnectionStrings["ASP.NET MVC"].ConnectionString;
        private readonly SqlConnection conn = new SqlConnection(cnstr);

        #region Member查詢單一商品評價
        public List<MembersReviewViewModel> GetItemReviews(string Item_Id)
        {
            Guid Item = Guid.Parse(Item_Id);
            List<MembersReviewViewModel> DataList = new List<MembersReviewViewModel>();
            string sql = $@"SELECT Members.Name,Reviews.* FROM Reviews INNER JOIN Members ON Reviews.Member_Id=Members.Member_Id
                            WHERE Item_Id='{Item}'";
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(sql, conn);
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    MembersReviewViewModel Data = new MembersReviewViewModel();
                    Data.Name = dr["Name"].ToString();
                    Data.Ratings = dr["Ratings"].ToString();
                    Data.Content = dr["Content"].ToString();
                    Data.CreateTime = Convert.ToDateTime(dr["CreateTime"]);
                    DataList.Add(Data);
                }

            }
            catch (Exception e)
            {
                throw new Exception(e.Message.ToString());
            }
            finally
            {
                conn.Close();
            }
            return DataList;
        }
        #endregion

        #region 新增評價
        public void AddReviews(Reviews Data, string Member_Id)
        {

            string sql = $@"INSERT INTO Reviews(Review_Id,Member_Id,Item_Id,Content,Ratings,CreateTime) VALUES 
                        ('{Guid.NewGuid().ToString()}','{Member_Id}','{Data.Item_Id}','{Data.Content}','{Data.Ratings}','{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")}');";
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.ExecuteNonQuery();

            }
            catch (Exception e)
            {
                throw new Exception(e.Message.ToString());
            }
            finally
            {
                conn.Close();
            }
        }
        #endregion

        #region 修改評價
        public void UpdateReviews(Reviews Data, string Member_Id)
        {

            string sql = $@"UPDATE Reviews SET Content='{Data.Content}',Ratings='{Data.Ratings}',CreateTime='{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")}'
                            WHERE Member_Id='{Member_Id}' AND Item_Id='{Data.Item_Id}'";

            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.ExecuteNonQuery();

            }
            catch (Exception e)
            {
                throw new Exception(e.Message.ToString());
            }
            finally
            {
                conn.Close();
            }
        }
        #endregion

        #region 查詢商品名稱
        public string ItemName(string Item_Id)
        {
            string Name;
            string sql = $@"SELECT Name From ItemInfo INNER JOIN Item ON ItemInfo.ItemInfo_Id=Item.ItemInfo_Id
                          WHERE Item_Id='{Item_Id}';";
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(sql, conn);
                SqlDataReader dr = cmd.ExecuteReader();
                dr.Read();
                Name = dr["Name"].ToString();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message.ToString());
            }
            finally
            {
                conn.Close();
            }
            return Name;

        }
        #endregion

        #region Admin查詢單一商品評價
        public List<AdminReviewViewModel> AdminItemReviews(string Item_Id)
        {
            Guid Item = Guid.Parse(Item_Id);
            List<AdminReviewViewModel> DataList = new List<AdminReviewViewModel>();
            string sql = $@"SELECT Members.Name,Reviews.*,Item.* FROM Reviews
                            INNER JOIN Members ON Reviews.Member_Id=Members.Member_Id
                            INNER JOIN Item ON Reviews.Item_Id=Item.Item_Id
                            WHERE Reviews.Item_Id='{Item}'";
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(sql, conn);
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    AdminReviewViewModel Data = new AdminReviewViewModel();
                    Data.Color = dr["Color"].ToString();
                    Data.Size = dr["Size"].ToString();
                    Data.Ratings = dr["Ratings"].ToString();
                    Data.Content = dr["Content"].ToString();
                    Data.Name = dr["Name"].ToString();
                    DataList.Add(Data);
                }

            }
            catch (Exception e)
            {
                throw new Exception(e.Message.ToString());
            }
            finally
            {
                conn.Close();
            }
            return DataList;
        }
        #endregion
    }

}