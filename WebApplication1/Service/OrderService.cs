﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data.SqlClient;
using WebApplication1.Models;
using WebApplication1.ViewModels;
namespace WebApplication1.Service
{
    public class OrderService
    {
        private readonly static string cnstr = ConfigurationManager.ConnectionStrings["ASP.NET MVC"].ConnectionString;
        private readonly SqlConnection conn = new SqlConnection(cnstr);

        #region 搜尋訂單
        public List<OrderViewModel> OrderSearch(string Search,ForPaging Paging)
        {
            SetMaxPaging(Search, Paging);
            string sql = $@"Select * From(Select ROW_NUMBER() Over(Order By o.OrderTime desc)as sort,o.*,m.Name From [Order] o Inner Join Members m On o.Member_Id=m.Member_Id Where o.Order_Id Like '%{Search}%' or m.Name Like '%{Search}%')base where base.sort Between {(Paging.NowPage - 1) * Paging.ItemNum + 1} and {Paging.NowPage * Paging.ItemNum}";
            List<OrderViewModel> DataList = new List<OrderViewModel>();
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(sql, conn);
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    OrderViewModel Data = new OrderViewModel();
                    Data.order.Order_Id = dr["Order_Id"].ToString();
                    Data.order.Member_Id = dr["Member_Id"].ToString();
                    Data.order.Total = Convert.ToInt32(dr["Total"]);
                    Data.order.Receiver = dr["Receiver"].ToString();
                    Data.order.Phone = dr["Phone"].ToString();
                    Data.order.Shipping_Id = Convert.ToInt32(dr["Shipping_Id"]);
                    Data.order.Payment_Id = Convert.ToInt32(dr["Payment_Id"]);
                    Data.order.Address = dr["Address"].ToString();
                    Data.order.OrderTime = Convert.ToDateTime(dr["OrderTime"]);
                    Data.order.ShippingTime = Convert.ToDateTime(dr["ShippingTime"]);
                    if (!dr["ShippingTime"].Equals(DBNull.Value))
                    {
                        Data.order.ShippingTime = Convert.ToDateTime(dr["ShippingTime"]);
                    }
                    if (!dr["PaymentTime"].Equals(DBNull.Value))
                    {
                        Data.order.PaymentTime = Convert.ToDateTime(dr["PaymentTime"]);
                    }
                    if (!dr["DeliveryTime"].Equals(DBNull.Value))
                    {
                        Data.order.DeliveryTime = Convert.ToDateTime(dr["DeliveryTime"]);
                    }
                    Data.order.Status_Id = Convert.ToInt32(dr["Status_Id"]);
                    Data.Name = dr["Name"].ToString();
                    DataList.Add(Data);
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message.ToString());
            }
            finally
            {
                conn.Close();
            }
            return DataList;
        }
        #endregion

        #region 根據狀態取得訂單陣列
        public List<OrderViewModel> GetOrderByStatus(int Status,ForPaging Paging) {
            SetMaxPaging(Status, Paging);
            string sql = $@"Select * From(Select ROW_NUMBER() Over(Order By OrderTime desc)as sort,o.*,m.Name From [Order] o Inner Join Members m On o.Member_Id=m.Member_Id Where o.Status_Id={Status})base where base.sort Between {(Paging.NowPage - 1) * Paging.ItemNum + 1} and {Paging.NowPage * Paging.ItemNum}";
            List<OrderViewModel> DataList = new List<OrderViewModel>();
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(sql, conn);
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    OrderViewModel Data = new OrderViewModel();
                    Data.order.Order_Id = dr["Order_Id"].ToString();
                    Data.order.Member_Id = dr["Member_Id"].ToString();
                    Data.order.Total = Convert.ToInt32(dr["Total"]);
                    Data.order.Receiver = dr["Receiver"].ToString();
                    Data.order.Phone = dr["Phone"].ToString();
                    Data.order.Shipping_Id = Convert.ToInt32(dr["Shipping_Id"]);
                    Data.order.Payment_Id = Convert.ToInt32(dr["Payment_Id"]);
                    Data.order.Address = dr["Address"].ToString();
                    Data.order.OrderTime = Convert.ToDateTime(dr["OrderTime"]);
                    if (!dr["ShippingTime"].Equals(DBNull.Value))
                    {
                        Data.order.ShippingTime = Convert.ToDateTime(dr["ShippingTime"]);
                    }
                    if (!dr["PaymentTime"].Equals(DBNull.Value))
                    {
                        Data.order.PaymentTime = Convert.ToDateTime(dr["PaymentTime"]);
                    }
                    if (!dr["DeliveryTime"].Equals(DBNull.Value))
                    {
                        Data.order.DeliveryTime = Convert.ToDateTime(dr["DeliveryTime"]);
                    }
                    Data.order.Status_Id = Convert.ToInt32(dr["Status_Id"]);
                    Data.Name = dr["Name"].ToString();
                    DataList.Add(Data);
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message.ToString());
            }
            finally {
                conn.Close();
            }
            return DataList;
        }
        #endregion

        #region 設定最大頁數(有搜尋值)
        public void SetMaxPaging(string Search, ForPaging Paging) {
            string sql = $@"Select * From [Order] o Inner Join Members m On o.Member_Id=m.Member_Id Where o.Order_Id Like '%{Search}%' or m.Name Like '%{Search}%'";
            int Row=0;
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(sql, conn);
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Row++;
                }
            }
            catch (Exception)
            {
                Row = 0;
            }
            finally {
                conn.Close();
            }
            Paging.MaxPage = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(Row) / Paging.ItemNum));
            Paging.SetRightPage();
        }
        #endregion

        #region 設定最大頁數(根據狀態)
        public void SetMaxPaging(int Status, ForPaging Paging)
        {
            string sql = $@"Select * From(Select ROW_NUMBER() Over(Order By OrderTime desc)as sort,o.* From [Order] o Inner Join Members m On o.Member_Id=m.Member_Id Where o.Status_Id={Status})base";
            int Row = 0;
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(sql, conn);
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Row++;
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message.ToString());
            }
            finally
            {
                conn.Close();
            }
            Paging.MaxPage = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(Row) / Paging.ItemNum));
            Paging.SetRightPage();
        }
        #endregion

        #region 修改訂單狀態(出貨)
        public void ShipOrder(UpdateOrder updateData)
        {
            string sql = $@"Update [Order] Set Status_Id={updateData.Status_Id},DeliveryTime='{DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}' Where Order_Id='{updateData.Order_Id}'";
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message.ToString());
            }
            finally
            {
                conn.Close();
            }
        }
        #endregion

        #region 修改訂單狀態(取消)
        public void CancelOrder(UpdateOrder updateData) {
            string sql = $@"Update [Order] Set Status_Id={updateData.Status_Id} Where Order_Id='{updateData.Order_Id}'";
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.ExecuteNonQuery();
            }
            catch (Exception e) {
                throw new Exception(e.Message.ToString()); 
            }
            finally {
                conn.Close();
            }
        }
        #endregion

        #region 確認訂單Id存在
        public bool CheckOrderId(string Order_Id) {
            Order Data = new Order();
            string sql = $@"Select* From [Order] Where Order_Id='{Order_Id}'";
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(sql, conn);
                SqlDataReader dr = cmd.ExecuteReader();
                dr.Read();
                Data.Order_Id = dr["Order_Id"].ToString();
            }
            catch (Exception)
            {
                Data = null;
            }
            finally {
                conn.Close();
            }
            return (Data != null);
        }
        #endregion

    }
}