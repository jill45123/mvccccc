﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data.SqlClient;
using WebApplication1.Models;
using WebApplication1.ViewModels;


namespace WebApplication1.Service
{
    public class ItemService
    {
        private readonly static string cnstr = ConfigurationManager.ConnectionStrings["ASP.NET MVC"].ConnectionString;
        private readonly SqlConnection conn = new SqlConnection(cnstr);


        #region 取得商品資訊陣列Id
        public List<int> GetAllItemInfo_Id(int Category, ForPaging paging)
        {
            List<int> DataList = new List<int>();
            if (Category != 0)
            {
                SetMaxPaging(Category, paging);
                DataList = GetItemInfo_Id(Category, paging);
            }
            else
            {
                SetMaxPaging(paging);
                DataList = GetItemInfo_Id(paging);
            }
            return DataList;
        }
        #endregion

        #region 取得商品資訊Id
        public List<int> GetItemInfo_Id(ForPaging paging) {
            string sql = $@"Select * From (select ROW_NUMBER() over(order by CreateTime desc)as sort,ItemInfo_Id From ItemInfo)m Where m.sort Between {(paging.NowPage - 1) * paging.ItemNum + 1} and {paging.NowPage * paging.ItemNum}";
            List<int> IdList = new List<int>();
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(sql, conn);
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    IdList.Add(Convert.ToInt32(dr["ItemInfo_Id"]));
                }
            }
            catch (Exception)
            {
                IdList = null;
            }
            finally {
                conn.Close();
            }
            return IdList;
        }
        #endregion

        #region 取得某類別商品資訊Id
        public List<int> GetItemInfo_Id(int Category, ForPaging paging) {
            string sql = $@"Select * From(Select *,row_number() over(order by base.ItemInfo_Id)as sort From(Select distinct a.ItemInfo_Id From ItemInfo a Inner Join Item b On a.ItemInfo_Id=b.ItemInfo_Id Where b.Category_Id={Category})base)m Where m.sort between {(paging.NowPage - 1) * paging.ItemNum + 1} and {paging.NowPage * paging.ItemNum}";
            List<int> IdList = new List<int>();
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(sql, conn);
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    IdList.Add(Convert.ToInt32(dr["ItemInfo_Id"]));
                }
            }
            catch (Exception)
            {
                IdList = null;
            }
            finally {
                conn.Close();
            }
            return IdList;
        }
        #endregion

        #region 取得搜尋值全部商品資訊Id
        public List<int> GetIdBasedOnSearch(string Search, ForPaging paging)
        {
            SetMaxPaging(Search, paging);
            List<int> IdList = new List<int>();
            string sql = $@"Select* From(Select*, row_number() over(order by base.ItemInfo_Id)as sort From(Select  distinct a.ItemInfo_Id From ItemInfo a Inner Join Item b On a.ItemInfo_Id = b.ItemInfo_Id Where a.Name Like '%{Search}%')base)m Where m.sort between { (paging.NowPage - 1) * paging.ItemNum + 1}
            and { paging.NowPage * paging.ItemNum}";
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(sql, conn);
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    IdList.Add(Convert.ToInt32(dr["ItemInfo_Id"]));
                }
            }
            catch (Exception)
            {
                IdList = null;
            }
            finally
            {
                conn.Close();
            }
            return IdList;
        }
        #endregion

        //分類
        #region 設定商品陣列最大頁數
        public void SetMaxPaging(int Category, ForPaging Paging) {
            string sql = $@"Select *,row_number() over(order by base.ItemInfo_Id)as sort From(Select  distinct a.ItemInfo_Id From ItemInfo a Inner Join Item b On a.ItemInfo_Id=b.ItemInfo_Id Where b.Category_Id={Category})base";
            int Row = 0;
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(sql, conn);
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Row++;
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message.ToString());
            }
            finally {
                conn.Close();
            }
            Paging.MaxPage = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(Row) / Paging.ItemNum));
            Paging.SetRightPage();
        }
        #endregion

        //無搜尋值
        #region 設定商品陣列最大頁數
        public void SetMaxPaging(ForPaging Paging)
        {
            string sql = $@"Select *,row_number() over(order by base.ItemInfo_Id)as sort From(Select  distinct a.ItemInfo_Id From ItemInfo a Inner Join Item b On a.ItemInfo_Id=b.ItemInfo_Id )base";
            int Row = 0;
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(sql, conn);
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Row++;
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message.ToString());
            }
            finally
            {
                conn.Close();
            }
            Paging.MaxPage = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(Row) / Paging.ItemNum));
            Paging.SetRightPage();
        }
        #endregion

        //有搜尋值
        #region 設定商品陣列最大頁數
        public void SetMaxPaging(String Search, ForPaging Paging)
        {
            string sql = $@"Select *,row_number() over(order by base.ItemInfo_Id)as sort From(Select  distinct a.ItemInfo_Id From ItemInfo a Inner Join Item b On a.ItemInfo_Id=b.ItemInfo_Id Where a.Name Like '%{Search}%')base";
            int Row = 0;
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(sql, conn);
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Row++;
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message.ToString());
            }
            finally
            {
                conn.Close();
            }
            Paging.MaxPage = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(Row) / Paging.ItemNum));
            Paging.SetRightPage();
        }
        #endregion

        #region 取得一筆商品資訊
        public ItemInfo GetItemInfoById(int Id) {
            ItemInfo Data = new ItemInfo();
            string sql = $@"Select * From ItemInfo Where ItemInfo_Id={Id}";
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(sql, conn);
                SqlDataReader dr = cmd.ExecuteReader();
                dr.Read();
                Data.ItemInfo_Id = Convert.ToInt32(dr["ItemInfo_Id"]);
                Data.Name = dr["Name"].ToString();
                Data.About = dr["About"].ToString();
                Data.Watch=Convert.ToInt32(dr["Watch"]);
                Data.CreateTime = Convert.ToDateTime(dr["CreateTime"]);
            }
            catch (Exception) {
                Data = null;
            }
            finally {
                conn.Close();
            }
            return Data;
        }
        #endregion

        #region 取得商品最低價
        public int GetItemLowestPrice(int Id) {
            string sql = $@"Select Price From Item Where ItemInfo_Id={Id} Order By Price asc";
            int Price;
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(sql, conn);
                SqlDataReader dr = cmd.ExecuteReader();
                dr.Read();
                Price = Convert.ToInt32(dr["Price"]);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message.ToString());
            }
            finally {
                conn.Close();
            }
            return Price;
        }
        #endregion

        #region 取得商品評價
        public List<Reviews> GetReviewsById(int Id) {
            string sql = $@"Select * From Reviews Where ItemInfo_Id='{Id}'";
            List<Reviews> DataList = new List<Reviews>();
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(sql, conn);
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Reviews Data = new Reviews();
                    Data.Review_Id = dr["Review_Id"].ToString();
                    Data.Member_Id = dr["Member_Id"].ToString();
                    Data.Item_Id = dr["Item_Id"].ToString();
                    Data.Content = dr["Content"].ToString();
                    Data.Ratings = dr["Ratings"].ToString();
                    Data.CreateTime = Convert.ToDateTime(dr["CreateTime"]);
                    DataList.Add(Data);
                }
            }
            catch (Exception)
            {
                DataList = null;
            }
            finally {
                conn.Close();
            }
            return DataList;
        }
        #endregion

        #region 取得所有商品規格List
        public List<Item> GetItemList(int Id) {
            string sql = $@"Select * From Item a inner join Category b on a.Category_Id=b.Category_Id Where ItemInfo_Id={Id} and ItemStatus_Id=1";
            List<Item> DataList= new List<Item>();
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(sql, conn);
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Item Data = new Item();
                    Data.Item_Id = dr["Item_Id"].ToString();
                    Data.ItemInfo_Id = Convert.ToInt32(dr["ItemInfo_Id"]);
                    Data.Color = dr["Color"].ToString();
                    Data.Size = dr["Size"].ToString();
                    Data.Price = Convert.ToInt32(dr["Price"]);
                    Data.Category_Id = Convert.ToInt32(dr["Category_Id"]);
                    Data.ItemStatus_Id = Convert.ToInt32(dr["ItemStatus_Id"]);
                    Data.category.Category_Id = Convert.ToInt32(dr["Category_Id"]);
                    Data.category.Category_Name = dr["Category_Name"].ToString();
                    DataList.Add(Data);
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message.ToString());
            }
            finally {
                conn.Close();
            }
            return DataList;
        }
        #endregion

        #region 取得所有商品List Id
        public List<string> GetItemIdList(int Id)
        {
            string sql = $@"Select Item_Id From Item Where ItemInfo_Id={Id}";
            List<string> DataList = new List<string>();
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(sql, conn);
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    DataList.Add(dr["Item_Id"].ToString());
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message.ToString());
            }
            finally
            {
                conn.Close();
            }
            return DataList;
        }
        #endregion

        #region 取得類別名稱
        public Category GetCategory_Name(int Id)
        {
            string sql = $@"Select * From Category Where Category_Id={Id}";
            Category category = new Category();
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(sql, conn);
                SqlDataReader dr = cmd.ExecuteReader();
                dr.Read();
                category.Category_Id = Convert.ToInt32(dr["Category_Id"]);
                category.Category_Name = dr["Category_Name"].ToString();
            }
            catch (Exception)
            {
                throw new Exception();
            }
            finally
            {
                conn.Close();
            }
            return category;
        }
        #endregion

        #region 取得類別陣列
        public List<string> GetCategoryList()
        {
            string sql = $@"Select * From Category";
            List<string> DataList = new List<string>();
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(sql, conn);
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    DataList.Add(dr["Category_Name"].ToString());
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message.ToString());
            }
            finally
            {
                conn.Close();
            }
            return DataList;
        }
        #endregion

        #region 取得商品顏色陣列
        public List<string> GetColorList(int ItemInfo_Id) {
            string sql = $@"Select distinct Color From Item Where ItemInfo_Id={ItemInfo_Id} and ItemStatus_Id=1";
            List<string> Color = new List<string>();
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(sql, conn);
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Color.Add(dr["Color"].ToString());
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message.ToString());
            }
            finally {
                conn.Close();
            }
            return Color;
        }
        #endregion

        #region 新增商品資訊
        public int InsertItemInfo(string Name,string About) {
            string sql = $@"Insert Into ItemInfo (Name,About,CreateTime) VALUES ('{Name}','{About}','{DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss")}')
                            select scope_identity() as ItemInfo_Id ";
            int ItemInfo_Id;
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(sql, conn);
                SqlDataReader dr = cmd.ExecuteReader();
                dr.Read();
                ItemInfo_Id = Convert.ToInt32(dr["ItemInfo_Id"]);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message.ToString());
            }
            finally {
                conn.Close();
            }
            return ItemInfo_Id;
        }
        #endregion

        #region 新增商品
        public void InsertItem(Item newData) {
            string sql = $@"Insert Into Item (Item_Id,ItemInfo_Id,Color,Size,Price,Category_Id,ItemStatus_Id) VALUES ('{Guid.NewGuid()}',{newData.ItemInfo_Id},'{newData.Color}','{newData.Size}',{newData.Price},{newData.Category_Id},{1})";
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message.ToString());
            }
            finally {
                conn.Close();
            }
        }
        #endregion

        #region 新增商品類別
        public void InsertItemCategory(string newCategory) {
            string sql = $@"Insert Into Category (Category) VALUES ('{newCategory}')";
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message.ToString());
            }
            finally {
                conn.Close();
            }
        }
        #endregion

        #region 增加商品觀看次數
        public void AddWatch(int ItemInfo_Id)
        {
            string sql = $@"Update ItemInfo Set Watch=Watch+1 Where ItemInfo_Id={ItemInfo_Id}";
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message.ToString());
            }
            finally
            {
                conn.Close();
            }
        }
        #endregion

        #region 修改商品資訊
        public void UpdateItemInfo(int ItemInfo_Id,string Name,string About) {
            string sql = $@"Update ItemInfo SET Name='{Name}', About='{About}' Where ItemInfo_Id={ItemInfo_Id}";
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(sql, conn);
                SqlDataReader dr = cmd.ExecuteReader();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message.ToString());
            }
            finally {
                conn.Close();
            }
        }
        #endregion

        #region 修改商品狀態
        public void ItemListRemove(int ItemInfo_Id) {
            string sql = $@"Update Item SET ItemStatus_Id={2} Where ItemInfo_Id='{ItemInfo_Id}'";
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message.ToString());
            }
            finally {
                conn.Close();
            }
        }
        #endregion

        #region 修改價錢
        public void UpdatePrice(int ItemInfo_Id, int Price) {
            string sql = $@"Update Item Set Price={Price} Where ItemInfo_Id={ItemInfo_Id}";
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message.ToString());
            }
            finally {
                conn.Close();
            }
        }
        #endregion

        #region 修改商品狀態(Color)
        public void ChangeItemStatus(int ItemInfo_Id, string Color,int ItemStatus_Id) {
            string sql = $@"Update Item Set ItemStatus_Id={ItemStatus_Id}  Where ItemInfo_Id={ItemInfo_Id} and Color='{Color}'";
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message.ToString());
            }
            finally {
                conn.Close();
            }
        }
        #endregion

        #region 確認商品是否存在
        public bool CheckItemById(string Item_Id) {
            string sql = $@"Select * From Item Where Item_Id='{Item_Id}'";
            Item Data = new Item();
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(sql, conn);
                SqlDataReader dr = cmd.ExecuteReader();
                dr.Read();
                Data.Item_Id = dr["Item_Id"].ToString();
                Data.ItemInfo_Id = Convert.ToInt32(dr["ItemInfo_Id"]);
                Data.Color = dr["Color"].ToString();
                Data.Size = dr["Size"].ToString();
                Data.Price = Convert.ToInt32(dr["Price"]);
                Data.Category_Id = Convert.ToInt32(dr["Category_Id"]);
                Data.ItemStatus_Id = Convert.ToInt32(dr["ItemStatus_Id"]);
            }
            catch (Exception)
            {
                Data = null;
            }
            finally {
                conn.Close();
            }
            return (Data != null);
        }
        #endregion

        #region 確認商品資訊是否存在
        public bool CheckItemInfoById(int ItemInfo_Id)
        {
            string sql = $@"Select * From ItemInfo Where ItemInfo_Id='{ItemInfo_Id}'";
            ItemInfo Data = new ItemInfo();
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(sql, conn);
                SqlDataReader dr = cmd.ExecuteReader();
                dr.Read();
                Data.ItemInfo_Id = Convert.ToInt32(dr["ItemInfo_Id"]);
                Data.Name = dr["Name"].ToString();
                Data.About = dr["About"].ToString();
                Data.Watch = Convert.ToInt32(dr["Watch"]);
                Data.CreateTime = Convert.ToDateTime(dr["CreateTime"]);
            }
            catch (Exception)
            {
                Data = null;
            }
            finally
            {
                conn.Close();
            }
            return (Data != null);
        }
        #endregion

        #region 確認商品顏色
        public bool CheckItemColor(int ItemInfo_Id,string Color) {
            string sql = $@"Select * From Item Where ItemInfo_Id={ItemInfo_Id} and Color='{Color}'";
            Item Data = new Item();
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(sql, conn);
                SqlDataReader dr = cmd.ExecuteReader();
                dr.Read();
                Data.ItemInfo_Id = Convert.ToInt32(dr["ItemInfo_Id"]);
            }
            catch (Exception) {
                Data = null;
            }
            finally {
                conn.Close();
            }
            return (Data != null) ;
        }
        #endregion

    }
}