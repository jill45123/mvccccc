﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data.SqlClient;
using WebApplication1.Models;
using WebApplication1.ViewModels;

namespace WebApplication1.Service
{
    public class ImageService
    {
        private readonly static string cnstr = ConfigurationManager.ConnectionStrings["ASP.NET MVC"].ConnectionString;
        private readonly SqlConnection conn = new SqlConnection(cnstr);

        #region 新增圖片
        public int InsertImage(Image newData)
        {
            int Id;
            string sql = $@"Insert Into Image (FileName,Url,Type_Id) VALUES ('{newData.FileName}','{newData.Url}',{newData.Type_Id})
                            select scope_identity() as Image_Id";
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(sql, conn);
                SqlDataReader dr = cmd.ExecuteReader();
                dr.Read();
                Id = Convert.ToInt32(dr["Image_Id"]);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message.ToString());
            }
            finally
            {
                conn.Close();
            }
            return Id;
        }
        #endregion

        #region 新增至商品圖片
        public void InsertItemImage(ItemImage newData) {
            string sql = $@"Insert Into ItemImage (Item_Id,Image_Id) VALUES ('{newData.Item_Id}',{newData.Image_Id})";
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.ExecuteNonQuery();
            }
            catch (Exception e) {
                throw new Exception(e.Message.ToString());
            }
            finally {
                conn.Close();
            }
        }
        #endregion

        #region 取得最新圖片Id
        public int GetLatestImageId() {
            int Id;
            string sql = $@"Select Top 1 * From Image order by Image_Id desc";
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(sql, conn);
                SqlDataReader dr = cmd.ExecuteReader();
                dr.Read();
                Id = Convert.ToInt32(dr["Image_Id"]);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message.ToString());
            }
            finally {
                conn.Close();
            }
            return Id;
        }
        #endregion

        #region 取得ItemBlock的Image
        public Image GetItemBlockImage(int Id)
        {
            string sql = $@" Select top 1 c.ItemInfo_Id,m.Image_Id,m.FileName,m.Url,m.Type_Id From
            (Select a.Item_Id,b.* from ItemImage a Inner Join Image b On a.Image_Id=b.Image_Id)m 
            Inner Join Item c On c.Item_Id=m.Item_Id Where c.ItemInfo_Id={Id}";
            Image Data = new Image();
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(sql, conn);
                SqlDataReader dr = cmd.ExecuteReader();
                dr.Read();
                Data.Image_Id = Convert.ToInt32(dr["Image_Id"]);
                Data.FileName = dr["FileName"].ToString();
                Data.Url = dr["Url"].ToString();
                Data.Type_Id = Convert.ToInt32(dr["Type_Id"]);
            }
            catch (Exception)
            {
                Data = null;
            }
            finally
            {
                conn.Close();
            }
            return Data;

        }
        #endregion

        #region 取得所有Image
        public List<Image> GetItemImageList(int Id) {
            string sql = $@"Select distinct m.Image_Id, c.ItemInfo_Id,m.FileName,m.Url,m.Type_Id From
            (Select a.Item_Id,b.* from ItemImage a Inner Join Image b On a.Image_Id=b.Image_Id)m 
            Inner Join Item c On c.Item_Id=m.Item_Id Where c.ItemInfo_Id={Id}";
            List<Image> DataList = new List<Image>();
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(sql, conn);
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Image Data = new Image();
                    Data.Image_Id = Convert.ToInt32(dr["Image_Id"]);
                    Data.FileName = dr["FileName"].ToString();
                    Data.Url = dr["Url"].ToString();
                    Data.Type_Id = Convert.ToInt32(dr["Type_Id"]);
                    DataList.Add(Data);
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message.ToString());
            }
            finally {
                conn.Close();
            }
            return DataList;
        }
        #endregion

    }
}