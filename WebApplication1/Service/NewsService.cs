﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data.SqlClient;
using WebApplication1.Models;
using WebApplication1.ViewModels;

namespace WebApplication1.Service
{
    public class NewsService
    {
        private readonly static string cnstr = ConfigurationManager.ConnectionStrings["ASP.NET MVC"].ConnectionString;
        private readonly SqlConnection conn = new SqlConnection(cnstr);

        #region 取得所有最新消息
        public List<NewsViewModel> GetDataList(ForPaging paging)
        {
            SetMaxPaging(paging);
            string sql = $@"Select * From(Select ROW_NUMBER() Over(Order By CreateTime desc)as sort,n.*,i.FileName,i.Type_Id,i.Url From News n Inner Join Image i On n.Image_Id=i.Image_Id)base Where base.sort Between {(paging.NowPage - 1) * paging.ItemNum + 1} and {paging.NowPage * paging.ItemNum}";
            List<NewsViewModel> DataList = new List<NewsViewModel>();
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(sql, conn);
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    NewsViewModel Data = new NewsViewModel();
                    Data.news.News_Id = Convert.ToInt32(dr["News_Id"]);
                    Data.news.Title = dr["Title"].ToString();
                    Data.news.Content = dr["Content"].ToString();
                    Data.news.CreateTime = Convert.ToDateTime(dr["CreateTime"]);
                    Data.news.Image_Id = Convert.ToInt32(dr["Image_Id"]);
                    Data.image.Image_Id= Convert.ToInt32(dr["Image_Id"]);
                    Data.image.FileName = dr["FileName"].ToString();
                    Data.image.Type_Id = Convert.ToInt32(dr["Type_Id"]);
                    Data.image.Url = dr["Url"].ToString();
                    DataList.Add(Data);
                }
            }
            catch (Exception)
            {
                DataList = null;
            }
            finally
            {
                conn.Close();
            }
            return DataList;
        }
        #endregion 

        #region 取得前三最新消息
        public List<News> GetTop3News()
        {
            string sql = $@"Select Top 3 * From News Order By CreateTime desc";
            List<News> DataList = new List<News>();
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(sql, conn);
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    News Data = new News();
                    Data.News_Id = Convert.ToInt32(dr["News_Id"]);
                    Data.Title = dr["Title"].ToString();
                    Data.Content = dr["Content"].ToString();
                    Data.CreateTime = Convert.ToDateTime(dr["CreateTime"]);
                    Data.Image_Id = Convert.ToInt32(dr["Image_Id"]);
                    DataList.Add(Data);
                }
            }
            catch (Exception)
            {
                DataList = null;
            }
            finally
            {
                conn.Close();
            }
            return DataList;
        }
        #endregion

        #region 取得一筆最新消息
        public NewsViewModel GetNewsById(int Id)
        {
            string sql = $@"Select * From News n Inner Join Image i On n.Image_Id=i.Image_Id Where News_Id={Id}";
            NewsViewModel Data = new NewsViewModel();
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(sql, conn);
                SqlDataReader dr = cmd.ExecuteReader();
                dr.Read();
                Data.news.News_Id = Convert.ToInt32(dr["News_Id"]);
                Data.news.Title = dr["Title"].ToString();
                Data.news.Content = dr["Content"].ToString();
                Data.news.CreateTime = Convert.ToDateTime(dr["CreateTime"]);
                Data.news.Image_Id = Convert.ToInt32(dr["Image_Id"]);
                Data.image.Image_Id = Convert.ToInt32(dr["Image_Id"]);
                Data.image.FileName = dr["FileName"].ToString();
                Data.image.Url = dr["Url"].ToString();
                Data.image.Type_Id = Convert.ToInt32(dr["Type_Id"]);
            }
            catch (Exception)
            {
                Data = null;
            }
            finally
            {
                conn.Close();
            }
            return Data;
        }
        #endregion

        #region 新增最新消息
        public void InsertNews(News newData)
        {
            string sql = $@"Insert Into News (Title,Content,CreateTime,Image_Id) VALUES ('{newData.Title}','{newData.Content}','{DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}',{newData.Image_Id})";
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message.ToString());
            }
            finally
            {
                conn.Close();
            }

        }
        #endregion

        #region 修改最新消息
        public void UpdateNews(UpdateNewsViewModel Data) {
            string sql = $@"Update News SET Title='{Data.Title}',Content='{Data.Content}' Where News_Id={Data.Id}";
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message.ToString());
            }
            finally {
                conn.Close();
            }
        }
        #endregion

        #region 刪除消息
        public void DeleteNews(int Id) {
            string sql = $@"Delete From News Where News_Id={Id}";
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message.ToString());
            }
            finally {
                conn.Close();
            }
        }
        #endregion

        #region 設定最大頁數
        public void SetMaxPaging(ForPaging Paging)
        {
            string sql = $@"Select * From News";
            int Row = 0;
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(sql, conn);
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Row++;
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message.ToString());
            }
            finally
            {
                conn.Close();
            }
            Paging.MaxPage = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(Row) / Paging.ItemNum));
            Paging.SetRightPage();
        }
        #endregion
    }
}