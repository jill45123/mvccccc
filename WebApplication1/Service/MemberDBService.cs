﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data.SqlClient;
using WebApplication1.Models;
using System.Security.Cryptography;
using System.Text;
using WebApplication1.Service;
using System.Web.Mvc;

namespace WebApplication1.Service
{
    public class MemberDBService
    {
        private readonly static string cnstr = ConfigurationManager.ConnectionStrings["ASP.NET MVC"].ConnectionString;
        private readonly SqlConnection conn = new SqlConnection(cnstr);

        #region 註冊
        public void Register(Members newMember)
        {
            newMember.Password = HashPassword(newMember.Password);
            string sql = $@"INSERT INTO Members(Member_Id,Account,Password,Name,Phone,Email,Address,BirthDate,AuthCode,IsAdmin) VALUES 
                            ('{Guid.NewGuid().ToString()}','{newMember.Account}','{newMember.Password}','{newMember.Name}','{newMember.Phone}','{newMember.Email}','{newMember.Address}','{newMember.BirthDate.ToString("yyyy-MM-dd")}','{newMember.AuthCode}','0');";
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.ExecuteNonQuery();

            }
            catch (Exception e)
            {
                throw new Exception(e.Message.ToString());
            }
            finally
            {
                conn.Close();
            }

        }
        #endregion
        
        #region Hash密碼
        public string HashPassword(string Password)
        {
            string saltkey = "1q2w3e4r5t6y7u8i9o0p";
            string saltAndPassword = string.Concat(Password, saltkey);
            SHA256CryptoServiceProvider sha256Hasher = new SHA256CryptoServiceProvider();
            byte[] PasswordData = Encoding.Default.GetBytes(saltAndPassword);
            byte[] HashData = sha256Hasher.ComputeHash(PasswordData);
            string HashResult = Convert.ToBase64String(HashData);
            return HashResult;
        }
        #endregion

        #region 查詢一筆資料
        public Members GetDataByMember_Id(string Member_Id)
        {
            Members Data = new Members();
            string sql = $@"SELECT * FROM Members WHERE Member_Id='{Member_Id}';";
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(sql, conn);
                SqlDataReader dr = cmd.ExecuteReader();
                dr.Read();
                Data.Member_Id = dr["Member_Id"].ToString();
                Data.Account = dr["Account"].ToString();
                Data.Password = dr["Password"].ToString();
                Data.Name = dr["Name"].ToString();
                Data.Phone = dr["Phone"].ToString();
                Data.Email = dr["Email"].ToString();
                Data.Address = dr["Address"].ToString();
                Data.BirthDate = Convert.ToDateTime(dr["BirthDate"]);
                Data.AuthCode = dr["AuthCode"].ToString();
                Data.IsAdmin = Convert.ToBoolean(dr["IsAdmin"]);
                Data.TokenRefresh = dr["TokenRefresh"].ToString();
            }
            catch (Exception e)
            {
                Data = null;
            }
            finally
            {
                conn.Close();
            }
            return Data;
        }
        #endregion

        #region 帳號重複註冊
        public bool AccountCheck(string Account)
        {
            Members Data = GetDataByAccount(Account);
            bool result = (Data == null);
            return result;
        }
        #endregion

        #region 信箱驗證
        public string EmailValidate(string Account, string AuthCode)
        {
            Members ValidateMember = GetDataByAccount(Account);
            string ValidateStr = string.Empty;
            if (ValidateMember != null)
            {
                if (ValidateMember.AuthCode == AuthCode)
                {
                    string sql = $@"UPDATE Members SET AuthCode='{string.Empty}' WHERE Account='{Account}';";
                    try
                    {
                        conn.Open();
                        SqlCommand cmd = new SqlCommand(sql, conn);
                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception e)
                    {
                        throw new Exception(e.Message.ToString());
                    }
                    finally
                    {
                        conn.Close();
                    }
                    ValidateStr = "";
                }
                else
                {
                    ValidateStr = "驗證碼錯誤，請重新確認或再註冊";
                }
            }
            else
            {
                ValidateStr = "傳送資料錯誤，請重新確認或再註冊";
            }

            return ValidateStr;

        }
        #endregion

        #region 登入確認
        public Members GetDataByAccount(string Account)
        {
            Members Data = new Members();
            string sql = $@"SELECT * FROM Members WHERE Account='{Account}';";
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(sql, conn);
                SqlDataReader dr = cmd.ExecuteReader();
                dr.Read();
                Data.Member_Id = dr["Member_Id"].ToString();
                Data.Account = dr["Account"].ToString();
                Data.Password = dr["Password"].ToString();
                Data.AuthCode = dr["AuthCode"].ToString();
            }
            catch (Exception e)
            {
                Data = null;
            }
            finally
            {
                conn.Close();
            }
            return Data;
        }
        public string LoginCheck(string Account, string Password)
        {
            Members LoginMember = GetDataByAccount(Account);
            if (LoginMember != null)
            {
                if (String.IsNullOrWhiteSpace(LoginMember.AuthCode))
                {
                    if (PasswordCheck(LoginMember, Password))
                    {
                         return "";
                    }
                    else
                    {
                        return "密碼輸入錯誤";
                    }
                }
                else
                {
                    return "此帳號尚未經過Email驗證";
                }
            }
            else
            {
                return "無此會員帳號，請先註冊";
            }

        }
        #endregion

        #region 密碼確認
        public bool PasswordCheck(Members CheckMember, string Password)
        {
           bool result= CheckMember.Password.Equals(HashPassword(Password));
            return result;
        }
        #endregion

        #region 修改密碼
        public string ChangePassword(string Member_Id, string Password, string newPassword)
        {
            Members LoginMember =GetDataByMember_Id(Member_Id);

            string ValidateStr;
            if (PasswordCheck(LoginMember, Password))
            {
                LoginMember.Password = HashPassword(newPassword);
                string sql = $@"UPDATE Members SET Password='{LoginMember.Password}' WHERE Member_Id='{Member_Id}';";
                try
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.ExecuteNonQuery();
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message.ToString());
                }
                finally
                {
                    conn.Close();
                }
                 ValidateStr="";

            }
            else
            {
                 ValidateStr="舊密碼輸入錯誤";
            }
            return ValidateStr;
        }
        #endregion

        #region 修改個資
        public string  UpdateMemberData([Bind(Include="Name,Phone,Address,Email")]Members newData) {
            string ValidateStr;
            string sql = $@"UPDATE Members SET Name='{newData.Name}',Phone='{newData.Phone}',Address='{newData.Address}',Email='{newData.Email}'
                            WHERE Member_Id='{newData.Member_Id}';";
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.ExecuteNonQuery();
                ValidateStr = "";
            }
            catch (Exception e)
            {

                ValidateStr = "修改失敗"+e.Message.ToString();
                throw new Exception(e.Message.ToString());
            }
            finally {
                conn.Close();
            }
            return ValidateStr;
        }
        #endregion

        #region 忘記密碼
        
        public Members GetDataByEmail(string Email){
            Members Data = new Members();
            string sql = $@"SELECT * FROM Members WHERE Email='{Email}';";
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(sql, conn);
                SqlDataReader dr = cmd.ExecuteReader();
                dr.Read();
                Data.Member_Id = dr["Member_Id"].ToString();
                Data.Account = dr["Account"].ToString();
                Data.Password = dr["Password"].ToString();
                Data.Name = dr["Name"].ToString();
                Data.Phone = dr["Phone"].ToString();
                Data.Email = dr["Email"].ToString();
                Data.Address = dr["Address"].ToString();
                Data.BirthDate = Convert.ToDateTime(dr["BirthDate"]);
                Data.AuthCode = dr["AuthCode"].ToString();
                Data.IsAdmin = Convert.ToBoolean(dr["IsAdmin"]);
            
                /*
                Data.Account = dr["Account"].ToString();
                Data.Password = dr["Password"].ToString();
                Data.AuthCode = dr["AuthCode"].ToString();
                */
               
            }
            catch (Exception e)
            {
                Data = null;
            }
            finally
            {
                conn.Close();
            }
            return Data;
        }
        public string ForgotPasswordCheck(string Email) {

            Members Data=GetDataByEmail(Email);

            if (Data != null) {
                if (String.IsNullOrWhiteSpace(Data.AuthCode))
                {
                    return "";
                }
                else {
                    return "信箱未經驗證，無法修改密碼，請重新註冊";
                }
            }
            else {
                return "查無此信箱，請先註冊";
            }
        }

        public void UpdateAuthCode(string Email,string AuthCode) {
            
            string sql = $@"UPDATE Members SET AuthCode='{AuthCode}'
                         WHERE Email='{Email}';";
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.ExecuteNonQuery();
            }
            catch (Exception e) {
                throw new Exception(e.Message.ToString());
            }
            finally{
                conn.Close();
            }
        }

        public string ForgotChangePassword(string Email,string AuthCode, string Password) {
            Members Data = GetDataByEmail(Email);
            string ValidateStr;
            if (Data.AuthCode == AuthCode) {

                string newPassword = HashPassword(Password);
                string sql =$@"UPDATE Members SET AuthCode='{string.Empty}',Password='{newPassword}' 
                              WHERE Email='{Email}';";
                    
                try
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand(sql.ToString(), conn);
                    cmd.ExecuteNonQuery();
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message.ToString());
                }
                finally
                {
                    conn.Close();
                }

                ValidateStr = "";

            }else
            {
                ValidateStr = "驗證碼錯誤，請重新確認或再註冊";
            }
            return ValidateStr;
        }
        #endregion

        #region 取得角色
        public string GetRole(string Member_Id)
        {
            string Role = "User";
            Members LoginMember = GetDataByMember_Id(Member_Id);
            if (LoginMember.IsAdmin)
            {
                Role += ",Admin";
            }
            return Role;

        }
        #endregion

        #region token_refresh

        //查詢是否符合資料庫中的token_refresh值
        public bool CheckTokenRefresh(string Member_Id,string tokenRefresh) {

            Members Data=new Members();
            string sql = $@"SELECT * FROM Members WHERE Member_Id='{Member_Id}' AND TokenRefresh='{tokenRefresh}';";
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(sql, conn);
                SqlDataReader dr = cmd.ExecuteReader();
                dr.Read();
                Data.Member_Id = dr["Member_Id"].ToString();
                Data.TokenRefresh= dr["TokenRefresh"].ToString();
            }
            catch (Exception e)
            {
                Data = null;
            }
            finally
            {
                conn.Close();
            }

            return (Data != null);
        }

        //新增
        public string AddTokenRefresh(string Member_Id)
        {
            string tokenRefresh = Guid.NewGuid().ToString();

            string sql = $@"UPDATE Members SET TokenRefresh='{tokenRefresh}' WHERE Member_Id='{Member_Id}';";
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message.ToString());
            }
            finally
            {
                conn.Close();
            }
            return tokenRefresh;
        }
        #endregion


    }
}