﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;

namespace WebApplication1.Service
{
    public class MailService
    {
        private string gmail_account = "8931angela@gmail.com";
        private string gmail_password = "8931xyun";
        private string gmail_email = "8931angela@gmail.com";

        #region 寄會員驗證信
        public string GetValidateCode()
        {
            string[] Code = { "A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z",
                              "0","1","2","3","4","5","6","7","8","9",
                              "a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"};
            Random rd = new Random();
            string ValidateCode = string.Empty;
            for (int i = 0; i < 10; i++)
            {
                ValidateCode += Code[rd.Next(Code.Count())];

            }
            return ValidateCode;
        }

        public string GetRegisterMailBody(string TempString, string UserName, string ValidateUrl)
        {
            TempString = TempString.Replace("{{UserName}}", UserName);
            TempString = TempString.Replace("{{ValidateUrl}}", ValidateUrl);
            return TempString;
        }


        public void SendRegisterMail(string MailSubject,string MailBody, string ToEmail)
        {
            SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");
            SmtpServer.Port = 587;
            SmtpServer.Credentials = new System.Net.NetworkCredential(gmail_account, gmail_password);
            SmtpServer.EnableSsl = true;

            MailMessage mail = new MailMessage();
            mail.From = new MailAddress(gmail_email);
            mail.To.Add(ToEmail);
            mail.Subject = MailSubject;
            mail.Body = MailBody;
            mail.IsBodyHtml = true;

            SmtpServer.Send(mail);

        }
        #endregion

        #region 忘記密碼
        public string GetForgotPasswordMailBody(string TempString, string AuthCode)
        {
            TempString = TempString.Replace("{{AuthCode}}", AuthCode);
            return TempString;
        }

        #endregion
    }
}