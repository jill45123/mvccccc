﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication1.Models;

namespace WebApplication1.ViewModels
{
    public class ItemViewModel
    {
        public ItemInfo itemInfo { get; set; }
        public List<Item> ItemList { get; set; }
        public List<Image> ImageList { get; set; }
        public List<Reviews> ReviewList { get; set; }

    }
}