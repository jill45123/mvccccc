﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication1.Models;

namespace WebApplication1.ViewModels
{
    public class NewsViewModel
    {
        public News news { get; set; } = new News();
        public Image image { get; set; } = new Image();
    }
}