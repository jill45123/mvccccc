﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.ViewModels
{
    public class MessageViewModel
    {
        public string Message { get; set; }
        public object Data { get; set; } 
    }
}