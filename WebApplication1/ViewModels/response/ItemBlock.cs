﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication1.Models;

namespace WebApplication1.ViewModels
{
    public class ItemBlock
    {
        public ItemInfo itemInfo { get; set; }
        public List<Item> itemList { get; set; }
        public int Pirce { get; set; }
        public Image image { get; set; }
    }
}