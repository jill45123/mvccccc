﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication1.Models;

namespace WebApplication1.ViewModels
{
    public class OrderViewModel
    {
        public Order order { get; set; } = new Order();
        public string Name { get; set; }
    }
}