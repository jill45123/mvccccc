﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.ViewModels
{
    public class AdminReviewViewModel
    {
        public string Ratings { get; set; }
        public string Color { get; set; }
        public string Size { get; set; }
        public string Content { get; set; }
        public string Name { get; set; }
    }
}