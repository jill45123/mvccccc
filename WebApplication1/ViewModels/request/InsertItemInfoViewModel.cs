﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.ViewModels { 
    public class InsertItemInfoViewModel
    {
        public string Name { get; set; }
        public string About { get; set; }
    }
}