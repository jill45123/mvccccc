﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.ViewModels
{
    public class UpdateItem
    {
        public int ItemInfo_Id { get; set; }
        public string Name { get; set; }
        public string About { get; set; }
        public int Price { get; set; }
        public int Category_Id { get; set; }
        public string Color_List { get; set; }
    }
}