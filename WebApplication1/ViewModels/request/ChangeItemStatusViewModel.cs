﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.ViewModels
{
    public class ChangeItemStatusViewModel
    {
        public string Item_Id { get; set; }
        public int ItemStatus_Id { get; set; }
    }
}