﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication1.Models;

namespace WebApplication1.ViewModels
{
    public class MembersReviewViewModel
    {
        public string Name { get; set; }
        public string Ratings { get; set; }
        public string Content { get; set; }
        public DateTime CreateTime { get; set; }

    }
}