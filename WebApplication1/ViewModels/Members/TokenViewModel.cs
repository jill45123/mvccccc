﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.ViewModels
{
    public class TokenViewModel
    {
        public string token { get; set; }
        public string tokenRefresh { get; set; }
    }
}