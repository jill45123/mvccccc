﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication1.ViewModels
{
    public class ForgotPasswordViewModel
    {

        public string Email { get; set; }

        [DisplayName("驗證碼")]
        [Required(ErrorMessage = "請輸入驗證碼")]
        public string AuthCode { get; set; }


        [DisplayName("新密碼")]
        [Required(ErrorMessage = "請輸入密碼")]
        public string Password { get; set; }


        [DisplayName("新密碼確認")]
        [Required(ErrorMessage = "請輸入密碼")]
        [Compare("Password", ErrorMessage = "兩次輸入不一致")]
        public string PasswordCheck { get; set; }
    }
}