﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.IO;
using System.Web;
using System.Threading.Tasks;
using WebApplication1.Models;
using WebApplication1.Service;
using WebApplication1.ViewModels;
namespace WebApplication1.Controllers
{
    public class OrderManagementController : ApiController
    {
        private MessageViewModel Result = new MessageViewModel();
        private OrderService orderService = new OrderService();

        //Get api/OrderManagement?Search={Search}&Page={Page}
        #region 搜尋訂單
        [HttpGet]
        [Route("api/OrderManagement")]
        public IHttpActionResult OrderSearch(string Search,int Page=1)
        {
            ForPaging Paging = new ForPaging(Page);
            List<OrderViewModel> DataList = new List<OrderViewModel>();
            DataList = orderService.OrderSearch(Search,Paging);
            if (DataList.Count!=0)
            {
                Result.Data = DataList;
                Result.Message = "搜尋成功！";
                return Content(HttpStatusCode.OK, Result);
            }
            else {
                Result.Message = "查無訂單";
                return Content(HttpStatusCode.BadRequest, Result);
            }

        }
        #endregion

        //Get api/OrderManagement?Status={Status_Id}&Page={Page}
        #region 根據狀態取得訂單陣列
        [HttpGet]
        [Route("api/OrderManagement")]
        public IHttpActionResult GetOrderByStatus(int Status,int Page=1) {
            ForPaging Paging = new ForPaging(Page);
            List<OrderViewModel> DataList = new List<OrderViewModel>();
            DataList = orderService.GetOrderByStatus(Status,Paging);
            if (DataList!=null)
            {
                Result.Data = DataList;
                Result.Message = "搜尋成功！";
                return Content(HttpStatusCode.OK, Result);
            }
            else {
                Result.Message = "查無訂單";
                return Content(HttpStatusCode.OK, Result);
            }
        }
        #endregion

        //Put api/OrderManagement/
        #region 更改訂單狀態
        [HttpPut]
        [Route("api/OrderManagement/")]
        public IHttpActionResult Put([FromBody]UpdateOrder updateData)
        {
            if (orderService.CheckOrderId(updateData.Order_Id))
            {
                if (updateData.Status_Id == 3)
                {
                    orderService.ShipOrder(updateData);
                    Result.Message = "訂單出貨成功！";
                    return Content(HttpStatusCode.OK, Result);
                }
                else if (updateData.Status_Id == 5)
                {
                    orderService.CancelOrder(updateData);
                    Result.Message = "訂單取消成功！";
                    return Content(HttpStatusCode.OK, Result);
                }
                else {
                    Result.Message = "狀態碼錯誤";
                    return Content(HttpStatusCode.BadRequest, Result);
                }
            }
            else
            {
                Result.Message = "訂單編號錯誤";
                return Content(HttpStatusCode.BadRequest, Result);
            }
        }
        #endregion

    }
}
