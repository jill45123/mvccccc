﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApplication1.Models;
using WebApplication1.Service;
using WebApplication1.ViewModels;

namespace WebApplication1.Controllers
{
    public class ReviewsController : ApiController
    {
        private readonly ReviewsDBService reviewsService = new ReviewsDBService();
        private readonly MessageViewModel Result = new MessageViewModel();
        private readonly ItemService itemDBService = new ItemService();
        private readonly ImageService imageService = new ImageService();

        #region 前台顯示單一商品總評價
        // Get api/Reviews/SelectItemReviews
        [HttpGet]
        [Route("api/Reviews/SelectItemReviews")]
        public IHttpActionResult SelectItemReviews([FromBody] Reviews Data)
        {
            Result.Message =reviewsService.ItemName(Data.Item_Id);
            Result.Data = reviewsService.GetItemReviews(Data.Item_Id);
            return Content(HttpStatusCode.OK, Result);
        }
        #endregion

        #region 會員新增評價
        // POST api/Reviews/InsertReviews
        [HttpPost]
        [Authorize]
        [Route("api/Reviews/InsertReviews")]
        public IHttpActionResult InsertReviews([FromBody] Reviews Data)
        {
            if (ModelState.IsValid)
            {
                string Member_Id = User.Identity.Name;
                reviewsService.AddReviews(Data, Member_Id);
                Result.Message = "新增評價成功";
                return Content(HttpStatusCode.OK, Result);
            }
            else
            {
                Result.Message = "頁面驗證失敗";
                return Content(HttpStatusCode.BadRequest, Result);
            }
        }
        #endregion

        #region 後台顯示單一商品總評價
        // Get api/Reviews/AdminItemReviews
        [HttpGet]
        [Route("api/Reviews/AdminItemReviews")]
        public IHttpActionResult AdminItemReviews([FromBody] Reviews Data)
        {
            Result.Message = reviewsService.ItemName(Data.Item_Id);
            Result.Data = reviewsService.AdminItemReviews(Data.Item_Id);
            return Content(HttpStatusCode.OK, Result);
        }
        #endregion
    }
}