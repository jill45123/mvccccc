﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.IO;
using System.Web.Http;
using WebApplication1.Service;
using WebApplication1.Models;
using WebApplication1.ViewModels;
using System.Web;
using System.Threading.Tasks;

namespace WebApplication1.Controllers
{
    public class NewsManagementController : ApiController
    {
        private readonly NewsService newsService = new NewsService();
        private readonly ImageService imageService = new ImageService();
        private MessageViewModel Result = new MessageViewModel();
        //POST api/NewsManagement
        #region 新增最新消息
        [HttpPost]
        [Route("api/NewsManagement")]
        public async Task<IHttpActionResult> Post()
        {
            if (!Request.Content.IsMimeMultipartContent())
            {
                Result.Message = "上傳非MIME 類別";
                return Content(HttpStatusCode.BadRequest, Result);
            }
            var root = HttpContext.Current.Server.MapPath("~/Upload");
            var Provider = new MultipartFormDataStreamProvider(root);
            await Request.Content.ReadAsMultipartAsync(Provider);
            News news = new News();
            foreach (var file in Provider.FileData)
            {
                Image image = new Image();
                image.Type_Id = 1;
                var name = file.Headers.ContentDisposition.FileName;
                image.FileName = DateTime.Now.ToString("yyyyMMddhhmmdd") + "_" + name.Trim('"');
                var localFileName = file.LocalFileName; //Url當地
                image.Url = Path.Combine(root, image.FileName); //Url新
                news.Image_Id = imageService.InsertImage(image);
                File.Move(localFileName, image.Url);
            }

            news.Title = HttpContext.Current.Request.Form["Title"];
            news.Content = HttpContext.Current.Request.Form["Content"];
            newsService.InsertNews(news);
            Result.Message = "新增成功";
            return Content(HttpStatusCode.OK, Result);
        }

        #endregion

        //Put api/NewsManagement
        #region 修改最新消息
        [HttpPut]
        [Route("api/NewsManagement")]
        public IHttpActionResult Put([FromBody] UpdateNewsViewModel Data)
        {
            newsService.UpdateNews(Data);
            Result.Message = "更新成功！";
            return Content(HttpStatusCode.OK, Result);
        }
        #endregion

        //DELETE api/NewsManagement?Id={Id}
        #region 刪除最新消息
        [HttpDelete]
        [Route("api/NewsManagement")]
        public IHttpActionResult Delete(int id)
        {
            newsService.DeleteNews(id);
            Result.Message = "刪除成功！";
            return Content(HttpStatusCode.OK, Result);
        }
        #endregion

    }
}
