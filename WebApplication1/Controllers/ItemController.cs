﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.IO;
using System.Web;
using System.Threading.Tasks;
using WebApplication1.Models;
using WebApplication1.Service;
using WebApplication1.ViewModels;

namespace WebApplication1.Controllers
{
    public class ItemController : ApiController
    {
        private readonly ItemService itemDBService = new ItemService();
        private readonly ImageService imageService = new ImageService();
        private MessageViewModel Result = new MessageViewModel();

        //GET api/Item/{Category?}/{Page?}
        #region 取得商品陣列
        [HttpGet]
        [Route("api/Item/{Category?}/{Page?}")]
        public IHttpActionResult ItemList(int Category=0,int Page=1) {
            ForPaging paging = new ForPaging(Page);
            List<ItemBlock> DataList = new List<ItemBlock>();
            List<int> IdList = itemDBService.GetAllItemInfo_Id(Category, paging);
            if (IdList.Count != 0)
            {
                foreach (var Id in IdList)
                {
                    ItemBlock Data = new ItemBlock();
                    Data.itemInfo = itemDBService.GetItemInfoById(Id);
                    Data.Pirce = itemDBService.GetItemLowestPrice(Id);
                    Data.image = imageService.GetItemBlockImage(Id);
                    Data.image.Url = Url.Content("~/Upload/" +Data.image.FileName);
                    DataList.Add(Data);
                }
                Result.Data = DataList;
                Result.Message = "查詢成功";
                return Content(HttpStatusCode.OK, Result);
            }
            else {
                Result.Message = "尚未有商品上架";
                return Content(HttpStatusCode.OK, Result);
            }
            
        }
        #endregion

        //Get api/Item/CategoryList
        #region 取得Category List
        [HttpGet]
        [Route("api/Item/CategoryList")]
        public IHttpActionResult GetCategory()
        {
            List<string> DataList = new List<string>();
            DataList = itemDBService.GetCategoryList();
            if (DataList == null)
            {
                Result.Message = "尚未有任何類別";
                return Content(HttpStatusCode.OK, Result);
            }
            else
            {
                Result.Data = DataList;
                return Content(HttpStatusCode.OK, Result);
            }
        }
        #endregion

        //Get api/Item?Id={Id}
        #region 取得商品規格陣列
        [HttpGet]
        [Route("api/Item")]
        public IHttpActionResult Item(int Id)
        {
            ItemViewModel DataList = new ItemViewModel();
            DataList.itemInfo = itemDBService.GetItemInfoById(Id);
            if (DataList.itemInfo == null)
            {
                Result.Message = "查無此商品";
                return Content(HttpStatusCode.BadRequest, Result);
            }
            else {
                itemDBService.AddWatch(Id);
                DataList.ReviewList = itemDBService.GetReviewsById(Id);
                DataList.ItemList = itemDBService.GetItemList(Id);
                DataList.ImageList = imageService.GetItemImageList(Id);
                foreach (var i in DataList.ImageList) { 
                    i.Url= Url.Content("~/Upload/" + i.FileName);
                }
                Result.Message = "查詢成功！";
                Result.Data = DataList;
                return Content(HttpStatusCode.OK, Result);
            }
        }
        #endregion

        //Get api/Item?Search={Search}&Page={Page}
        #region 搜尋商品
        [HttpGet]
        [Route("api/Item")]
        public IHttpActionResult ItemSearch(string Search,int Page=1)
        {
            ForPaging paging = new ForPaging(Page);
            List<ItemBlock> DataList = new List<ItemBlock>();
            List<int> IdList = itemDBService.GetIdBasedOnSearch(Search,paging);
            foreach (var Id in IdList)
            {
                ItemBlock Data = new ItemBlock();
                Data.itemInfo = itemDBService.GetItemInfoById(Id);
                Data.image = imageService.GetItemBlockImage(Id);
                Data.image.Url= Url.Content("~/Upload/" + Data.image.FileName);
                DataList.Add(Data);
            }
            Result.Data = DataList;
            if (DataList == null)
            {
                Result.Message = "查詢無結果";
                return Content(HttpStatusCode.OK, Result);
            }
            else
            {
                Result.Message = "查詢成功";
                return Content(HttpStatusCode.OK, Result);
            }
        }
        #endregion

    }
}