﻿using System;
using System.Linq;
using System.Net;
using System.Web.Http;
using WebApplication1.Service;
using WebApplication1.ViewModels;
using WebApplication1.Models;
using System.Web;
using WebApplication1.Security;
using System.Web.Configuration;
using Jose;
using System.Text;
using System.Collections.Generic;
using System.Web.Http.Cors;

namespace WebApplication1.Controllers
{
    //[EnableCors(origins: "*", headers: "*", methods: "*")]
    public class MembersController : ApiController
    {
        private readonly MemberDBService membersService = new MemberDBService();
        private readonly MailService mailService = new MailService();
        private readonly MessageViewModel Result = new MessageViewModel();
        private readonly JwtService jwtService = new JwtService();

        #region 註冊
        // POST api/Members/Register
        [HttpPost]
        [Route("api/Members/Register")]
        public IHttpActionResult Register([FromBody] MembersRegisterViewModel RegisterMember)
        {
            if (ModelState.IsValid)
            {
                if (membersService.AccountCheck(RegisterMember.newMember.Account))
                {
                    RegisterMember.newMember.Password = RegisterMember.Password;
                    string AuthCode = mailService.GetValidateCode();
                    RegisterMember.newMember.AuthCode = AuthCode;
                    membersService.Register(RegisterMember.newMember);

                    string TempMail = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("~/Views/Shared/RegisterEmailTemplate.html"));

                    var ValidateUrl = new Uri(Url.Link("EmailValidate", new { Account = RegisterMember.newMember.Account, AuthCode = AuthCode }));

                    /*
                    UriBuilder ValidateUrl = new UriBuilder(Request.Url)
                    { Path = Url.Action("EmailValidate", "Members", new { Account = RegisterMember.newMember.Account, AuthCode = AuthCode }) };
                    */

                    string MailBody = mailService.GetRegisterMailBody(TempMail, RegisterMember.newMember.Name, ValidateUrl.ToString().Replace("%3F", "?"));

                    mailService.SendRegisterMail("會員註冊驗證信", MailBody, RegisterMember.newMember.Email);
                    Result.Message = "註冊成功，請去收信以驗證Email";
                    return Content(HttpStatusCode.OK,Result);
                }
                else
                {
                    Result.Message = "此帳號已被註冊";
                    return Content(HttpStatusCode.BadRequest, Result);
                }
            }
            else {
                RegisterMember.Password = null;
                RegisterMember.PasswordCheck = null;
                Result.Message = "頁面驗證失敗";
                return Content(HttpStatusCode.BadRequest, Result);
            }
            
        }
        #endregion

        #region Email驗證
        // GET api/Members/EmailValidate
        [HttpGet]
        [AllowAnonymous]
        [Route("api/Members",Name="EmailValidate")]
        public IHttpActionResult EmailValidate(string Account, string AuthCode)
        {
           string ValidateStr= membersService.EmailValidate(Account, AuthCode);

            if (String.IsNullOrEmpty(ValidateStr))
            {
                Result.Message = "信箱驗證成功，現在可以登入了";
                return Content(HttpStatusCode.OK, Result.Message);
            }
            else {
                Result.Message = ValidateStr;
                return Content(HttpStatusCode.BadRequest, Result.Message);
            }
        }
        #endregion

        #region 登入
        //POST api/Members/AdminLogin
        [HttpPost]
        [Route("api/Members/AdminLogin")]
        public IHttpActionResult AdminLogin([FromBody] MembersLoginViewModel LoginMember)
        {
            string ValidateStr = membersService.LoginCheck(LoginMember.Account, LoginMember.Password);
            if (String.IsNullOrWhiteSpace(ValidateStr))
            {
                Members Data = membersService.GetDataByAccount(LoginMember.Account);
                string RoleData = membersService.GetRole(Data.Member_Id);
               
                if (RoleData == "User,Admin")
                {
                    string token = jwtService.GenerateToken(Data.Member_Id, RoleData);
                    string tokenRefresh = membersService.AddTokenRefresh(Data.Member_Id);

                    TokenViewModel tokenData = new TokenViewModel();
                    tokenData.token = token;
                    tokenData.tokenRefresh = tokenRefresh;
                    Result.Data = tokenData;
                    Result.Message = "登入成功，產生token";
                    return Content(HttpStatusCode.OK, Result);
                }
                else
                {
                    Result.Message = "角色錯誤，登入失敗";
                    return Content(HttpStatusCode.OK, Result);
                }
            }
            else
            {
                Result.Message = ValidateStr;
                return Content(HttpStatusCode.BadRequest, Result);
            }
        }


        //POST api/Members/Login
        [HttpPost]
        [Route("api/Members/Login")]
        public IHttpActionResult Login([FromBody] MembersLoginViewModel LoginMember)
        {
            string ValidateStr = membersService.LoginCheck(LoginMember.Account, LoginMember.Password);
            if (String.IsNullOrWhiteSpace(ValidateStr))
            {
                Members Data = membersService.GetDataByAccount(LoginMember.Account);
                string RoleData = membersService.GetRole(Data.Member_Id);

                string token = jwtService.GenerateToken(Data.Member_Id, RoleData);
                string tokenRefresh=membersService.AddTokenRefresh(Data.Member_Id);

                TokenViewModel tokenData = new TokenViewModel();
                tokenData.token = token;
                tokenData.tokenRefresh = tokenRefresh;
                Result.Data = tokenData;

                Result.Message = "登入成功，產生token";
                return Content(HttpStatusCode.OK, Result);

                /*
                string cookieName = WebConfigurationManager.AppSettings["CookieName"].ToString();
                HttpCookie cookie = new HttpCookie(cookieName);
                HttpContext.Current.Response.Cookies.Add(cookie);
                HttpContext.Current.Response.Cookies[cookieName].Expires= DateTime.Now.AddMinutes(Convert.ToInt32(WebConfigurationManager.AppSettings["ExpireMinutes"]));

                Response.Cookies.Add(cookie);
                Response.Cookies[cookieName].Expires = DateTime.Now.AddMinutes(Convert.ToInt32(WebConfigurationManager.AppSettings["ExpireMinutes"]));
                */

            }
            else
            {
                Result.Message = ValidateStr;
                return Content(HttpStatusCode.BadRequest, Result);
            }
        }
        #endregion

        #region 更新token
        [HttpPost]
        [Authorize]
        [Route("api/Members/Refresh")]
        public IHttpActionResult Refresh([FromBody] TokenViewModel Data)
        {
            if (membersService.CheckTokenRefresh(User.Identity.Name, Data.tokenRefresh))
            {
                string RoleData = membersService.GetRole(User.Identity.Name);
                string newtoken = jwtService.GenerateToken(User.Identity.Name, RoleData);
                string newtokenRefresh = membersService.AddTokenRefresh(User.Identity.Name);

                TokenViewModel tokenData = new TokenViewModel();
                tokenData.token = newtoken;
                tokenData.tokenRefresh = newtokenRefresh;
                Result.Data = tokenData;
                Result.Message = "產生newtoken";
                return Content(HttpStatusCode.OK, Result);

            }
            else
            {
                Result.Message = "Token Refresh值錯誤";
                return Content(HttpStatusCode.BadRequest, Result);

            }
        }
        #endregion

        #region 取得會員資料
        //GET api/Members/GetMemberData
        [HttpGet]
        [Authorize]
        [Route("api/Members/GetMemberData")]
        public IHttpActionResult GetMemberData()
        {
            Members Data = membersService.GetDataByMember_Id(User.Identity.Name);
            Result.Message = "查詢成功";
            Result.Data = Data;
            return Content(HttpStatusCode.OK, Result);
        }
        #endregion

        #region 變更密碼
        //PUT api/Members/ChangePassword
        [HttpPut]
        [Authorize]
        [Route("api/Members/ChangePassword")]
        public IHttpActionResult ChangePassword([FromBody] ChangePasswordViewModel Data) {
            if (ModelState.IsValid)
            {
                string ValidateStr = membersService.ChangePassword(User.Identity.Name, Data.Password, Data.newPassword);
                if (String.IsNullOrEmpty(ValidateStr))
                {
                    Result.Message = "密碼修改成功";
                    return Content(HttpStatusCode.OK, Result);
                }
                else
                {
                    Result.Message = ValidateStr;
                    return Content(HttpStatusCode.BadRequest, Result);
                }
            }
            else {
                Result.Message = "頁面驗證失敗";
                return Content(HttpStatusCode.BadRequest, Result);
            }
        }
        #endregion

        #region 變更個資
        //PUT api/Members/UpdateMemberData
        [HttpPut]
        [Authorize]
        [Route("api/Members/UpdateMemberData")]
        public IHttpActionResult UpdateMemberData([FromBody] Members newData)
        {
            newData.Member_Id = User.Identity.Name;
            string ValidateStr=membersService.UpdateMemberData(newData);
            if (String.IsNullOrEmpty(ValidateStr))
            {
                Result.Message = "修改成功";
                return Content(HttpStatusCode.OK, Result);
            }
            else {
                Result.Message = ValidateStr;
                return Content(HttpStatusCode.BadRequest, Result);
            }
            
        }
        #endregion

        #region 忘記密碼
        //PUT api/Members/ResendEmail
        [HttpPut]
        [Route("api/Members/ResendEmail")]
        public IHttpActionResult ResendEmail([FromBody] ForgotPasswordViewModel Data)
        {
            string ValidateStr = membersService.ForgotPasswordCheck(Data.Email);

            if (String.IsNullOrEmpty(ValidateStr))
            {
                string AuthCode = mailService.GetValidateCode();
                membersService.UpdateAuthCode(Data.Email, AuthCode);
                string TempMail = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("~/Views/Shared/ForgotPasswordEmailTemplate.html"));
                string MailBody = mailService.GetForgotPasswordMailBody(TempMail, AuthCode);

                mailService.SendRegisterMail("會員重設密碼驗證信", MailBody, Data.Email);
                Result.Message = "請到Email收驗證碼";
                return Content(HttpStatusCode.OK, Result);
            }
            else
            {
                Result.Message = ValidateStr;
                return Content(HttpStatusCode.BadRequest, Result);
            }
        }


        //PUT api/Members/ForgotPassword
        [HttpPut]
        [Route("api/Members/ForgotPassword")]
        public IHttpActionResult ForgotPassword([FromBody] ForgotPasswordViewModel Data)
        {
            if (ModelState.IsValid)
            {
                string ValidateStr = membersService.ForgotChangePassword(Data.Email, Data.AuthCode, Data.Password);

                if (String.IsNullOrEmpty(ValidateStr))
                {

                    Result.Message = "信箱驗證成功，現在可以登入了";
                    return Content(HttpStatusCode.OK, Result);
                }
                else
                {
                    Result.Message = ValidateStr;
                    return Content(HttpStatusCode.BadRequest, Result);
                }

            }
            else {
                Result.Message = "資料驗證錯誤";
                return Content(HttpStatusCode.BadRequest, Result);
            }
            
        }
        #endregion

        #region 登出
        //POST api/Members/Logout
        [HttpPost]
        [Authorize]
        [Route("api/Members/Logout")]
        public IHttpActionResult Logout()
        {

            /*
            httpRequest.Headers["Authorization"]=null;
            HttpContext.Current.Response.Headers.Remove("Members");
            */
            JwtService jwtService = new JwtService();
            string token = jwtService.RemoveToken();
            HttpRequest httpRequest = HttpContext.Current.Request;
            httpRequest.Headers["Authorization"] = token;
            Result.Message = "登出成功";
            Result.Data = token;
            return Content(HttpStatusCode.OK, Result);


            /*
            string cookieName = WebConfigurationManager.AppSettings["CookieName"].ToString();
            HttpCookie cookie = new HttpCookie(cookieName);
            cookie.Values.Clear();
            cookie.Expires = DateTime.Now.AddDays(-1);
            HttpContext.Current.Response.Cookies.Set(cookie);
            //Response.Cookies.Set(cookie);
            */

        }
        #endregion

        #region test User.Identity.Name
        //PUT api/Members/Test
        [HttpPut]
        [Authorize]
        [Route("api/Members/Test")]
        public IHttpActionResult Test()
        {
            string Data = User.Identity.Name;
            return Content(HttpStatusCode.OK, Data);

        }
        #endregion




    }
}