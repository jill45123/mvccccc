﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.IO;
using System.Web;
using System.Threading.Tasks;
using WebApplication1.Models;
using WebApplication1.Service;
using WebApplication1.ViewModels;

namespace WebApplication1.Controllers
{
    public class ItemManagementController : ApiController
    {
        private readonly ItemService itemDBService = new ItemService();
        private readonly ImageService imageService = new ImageService();
        private MessageViewModel Result = new MessageViewModel();

        //Get api/ItemManagement/GetItemList
        #region 取得全部商品(含ItemInfo,Item,Image)
        [HttpGet]
        [Route("api/ItemManagement/GetItemBlock")]
        public IHttpActionResult GetItemBlock(int Page=1) {
            ForPaging paging = new ForPaging(Page);
            List<ItemBlock> DataList = new List<ItemBlock>();
            List<int> IdList = itemDBService.GetItemInfo_Id(paging);
            foreach (var Id in IdList)
            {
                ItemBlock Data = new ItemBlock();
                Data.itemInfo = itemDBService.GetItemInfoById(Id);
                Data.itemList = itemDBService.GetItemList(Id);
                Data.Pirce = itemDBService.GetItemLowestPrice(Id);
                Data.image = imageService.GetItemBlockImage(Id);
                Data.image.Url= Url.Content("~/Upload/" + Data.image.FileName);
                DataList.Add(Data);
            }
            Result.Data = DataList;
            if (DataList == null)
            {
                Result.Message = "查無商品";
                return Content(HttpStatusCode.BadRequest, Result);
            }
            else
            {
                return Content(HttpStatusCode.OK, Result);
            }
        }
        #endregion

        //Post api/ItemManagement/InsertItem
        #region 新增商品
        [HttpPost]
        [Route("api/ItemManagement/InsertItem")]
        public async Task<IHttpActionResult> InsertItem()
        {
            var root = HttpContext.Current.Server.MapPath("~/Upload");
            var Provider = new MultipartFormDataStreamProvider(root);
            await Request.Content.ReadAsMultipartAsync(Provider);
            Item item = new Item();
            //新增ItemInfo
            item.ItemInfo_Id=itemDBService.InsertItemInfo(HttpContext.Current.Request.Form["Name"], HttpContext.Current.Request.Form["About"]);
            //新增Item
            string Color_array = HttpContext.Current.Request.Form["Color_array"];
            string[] Color = Color_array.Split(',');
            List<string> SizeList = new List<string> { "S", "M", "L", "XL", "2XL" };    
            item.Price = Convert.ToInt32(HttpContext.Current.Request.Form["Price"]);
            item.Category_Id = Convert.ToInt32(HttpContext.Current.Request.Form["Category_Id"]);
            foreach (string color in Color)
            {
                item.Color = color;
                foreach (string size in SizeList)
                {
                    item.Size = size;
                    itemDBService.InsertItem(item);
                }
            }
            foreach (var file in Provider.FileData)//新稱Image&ItemImage
            {
                Image image = new Image();
                image.Type_Id = 2;
                var name = file.Headers.ContentDisposition.FileName;
                image.FileName = DateTime.Now.ToString("yyyyMMddhhmmss") + "_" + name.Trim('"');
                var localFileName = file.LocalFileName; //Url當地
                image.Url = Path.Combine(root, image.FileName); //Url新
                ItemImage itemImage = new ItemImage();
                itemImage.Image_Id = imageService.InsertImage(image);
                File.Move(localFileName, image.Url);
                List<string> Item_IdList = itemDBService.GetItemIdList(item.ItemInfo_Id);
                foreach (string Id in Item_IdList)
                {
                    itemImage.Item_Id = Id;
                    imageService.InsertItemImage(itemImage);
                }
            }
            return Content(HttpStatusCode.OK, "上傳成功");
        }
        #endregion

        //Post api/ItemManagement/InsertItemCategory
        #region 新增商品類別
        [HttpPost]
        [Route("api/ItemManagement/InsertItemCategory")]
        public IHttpActionResult InsertItemCategory(string newCategory)
        {
            itemDBService.InsertItemCategory(newCategory);
            Result.Message = "新增成功！";
            return Content(HttpStatusCode.OK, Result);
        }
        #endregion

        //Put api/ItemManagement/Update
        #region 修改商品
        [HttpPut]
        [Route("api/ItemManagement/Update")]
        public IHttpActionResult Update([FromBody]UpdateItem newData)
        {
            if (itemDBService.CheckItemInfoById(newData.ItemInfo_Id))
            {
                itemDBService.UpdateItemInfo(newData.ItemInfo_Id,newData.Name,newData.About);
                List<string> Color = itemDBService.GetColorList(newData.ItemInfo_Id);

                string[] newColor = newData.Color_List.Split(',');
                foreach(string new_c in newColor)//Add
                {
                    if (!Color.Contains(new_c) && itemDBService.CheckItemColor(newData.ItemInfo_Id, new_c)){
                        itemDBService.ChangeItemStatus(newData.ItemInfo_Id, new_c, 1);
                    }
                    else if (!Color.Contains(new_c)) {
                        Item newItem = new Item();
                        newItem.ItemInfo_Id = newData.ItemInfo_Id;
                        newItem.Color = new_c;
                        newItem.Price = newData.Price;
                        newItem.Category_Id = newData.Category_Id;
                        List<string> SizeList = new List<string> { "S", "M", "L", "XL", "2XL" };
                        foreach (string size in SizeList)
                        {
                            newItem.Size = size;
                            itemDBService.InsertItem(newItem);
                        }
                    }
                }
                for (int i = 0; i < Color.Count; i++)//Remove
                {
                    if (!newColor.Contains(Color[i]))
                    {
                        itemDBService.ChangeItemStatus(newData.ItemInfo_Id, Color[i],2);
                    }
                }
                itemDBService.UpdatePrice(newData.ItemInfo_Id, newData.Price);
                Result.Message = "修改成功！";
                return Content(HttpStatusCode.OK, Result);
            }
            else {
                Result.Message = "ItemInfo_Id錯誤";
                return Content(HttpStatusCode.BadRequest, Result);
            }
        }
        #endregion

        //Put api/ItemManagement/Remove?Id={Id}
        #region 商品下架
        [HttpPut]
        [Route("api/ItemManagement/Remove")]
        public IHttpActionResult ChangeItemStatus(int Id) //ItemInfo_Id
        {
            if (itemDBService.CheckItemInfoById(Id))
            {
                itemDBService.ItemListRemove(Id);
                Result.Message = "下架成功！";
                return Content(HttpStatusCode.OK, Result);
            }
            else
            {
                Result.Message = "商品Id錯誤";
                return Content(HttpStatusCode.BadRequest, Result);
            }

        }
        #endregion
    }
}
