﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.IO;
using System.Web.Http;
using WebApplication1.Service;
using WebApplication1.Models;
using WebApplication1.ViewModels;
using System.Web;
using System.Threading.Tasks;

namespace WebApplication1.Controllers
{
    public class NewsController : ApiController
    {
        private readonly NewsService newsService = new NewsService();
        private readonly ImageService imageService = new ImageService();
        private MessageViewModel Result = new MessageViewModel();


        //GET api/News?Page={Page}
        #region 取得所有最新消息
        [HttpGet]
        [Route("api/News")]
        public IHttpActionResult GetNewsList(int Page=1)
        {
            List<NewsViewModel> Data = new List<NewsViewModel>();
            ForPaging Paging = new ForPaging(Page);
            Data= newsService.GetDataList(Paging);
            if (Data == null)
            {
                Result.Message = "尚未有最新消息";
                return Content(HttpStatusCode.BadRequest, Result);
            }
            else {
                foreach (var i in Data) { 
                    i.image.Url= Url.Content("~/Upload/" + i.image.FileName);
                }
                Result.Data = Data;
                return Content(HttpStatusCode.OK, Result);
            }
            
        }
        #endregion

        //Get api/News?Id={Id}
        #region 取得一筆最新消息
        [HttpGet]
        [Route("api/News")]
        public IHttpActionResult GetNews(int Id)
        {
            NewsViewModel Data = new NewsViewModel();
            Data = newsService.GetNewsById(Id);
           
            if (Data == null)
            {
                Result.Message = "查無訊息";
                return Content(HttpStatusCode.BadRequest, Result);
            }
            else
            {
                Data.image.Url = Url.Content("~/Upload/" + Data.image.FileName);
                Result.Data = Data;
                return Content(HttpStatusCode.OK, Result);
            }
        }
        #endregion
    }
}