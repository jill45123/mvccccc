﻿using Jose;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Configuration;

namespace WebApplication1.Security
{
    public class JwtService
    {
        #region 製作Token
        public string GenerateToken(string Member_Id, string Role)
        {
            JwtObject jwtObject = new JwtObject
            {
                Member_Id = Member_Id,
                Role = Role,
                Expire = DateTime.Now.AddMinutes(Convert.ToInt32(WebConfigurationManager.AppSettings["ExpireMinutes"])).ToString()
            };
            string SecretKey = WebConfigurationManager.AppSettings["SecretKey"].ToString();
            var payload = jwtObject;
            var token = JWT.Encode(payload, Encoding.UTF8.GetBytes(SecretKey), JwsAlgorithm.HS512);

            return token;

        }
        #endregion

        

        #region 移除token
        public string RemoveToken()
        {
            JwtObject jwtObject = new JwtObject
            {
                Expire = DateTime.Now.AddDays(-1).ToString()
            };
            string SecretKey = WebConfigurationManager.AppSettings["SecretKey"].ToString();
            var payload = jwtObject;
            var token = JWT.Encode(payload, Encoding.UTF8.GetBytes(SecretKey), JwsAlgorithm.HS512);
            return token;

        }
        #endregion

    }
}