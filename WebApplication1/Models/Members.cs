﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace WebApplication1.Models
{
    public class Members
    {
        public string Member_Id { get; set; }

        [Required(ErrorMessage = "請輸入帳號")]
        [StringLength(30, MinimumLength = 6, ErrorMessage = "帳號長度需介於6-30字元")]
        public string Account { get; set; }
        public string Password { get; set; }

        [Required(ErrorMessage = "請輸入姓名")]
        [StringLength(20, ErrorMessage = "姓名長度最多20字元")]
        public string Name { get; set; }

        [Required(ErrorMessage = "請輸入電話")]
        [StringLength(20, ErrorMessage = "電話長度最多10字元")]
        public string Phone { get; set; }

        [Required(ErrorMessage = "請輸入Email")]
        [StringLength(200, ErrorMessage = "Email長度最多200字元")]
        [EmailAddress(ErrorMessage = "這不是Email格式")]
        public string Email { get; set; }

        public string Address { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime BirthDate { get; set; }

        public string AuthCode { get; set; }

        public bool IsAdmin { get; set; }

        public string TokenRefresh { get; set; }
    }
}