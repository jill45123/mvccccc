﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class Item
    {
        public string Item_Id { get; set; }
        public int ItemInfo_Id{ get; set; }
        public string Color { get; set; }
        public string Size { get; set; }
        public int Price { get; set; }
        public int Category_Id { get; set; }
        public int ItemStatus_Id { get; set; }

        public Category category { get; set; } = new Category();
    }
}