﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class ItemStatus
    {
        public int ItemStatus_Id { get; set; }
        public string ItemStatus_Name { get; set; }
    }
}