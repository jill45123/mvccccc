﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class ItemInfo
    {
        public int ItemInfo_Id { get; set; }
        public string Name { get; set; }
        public string About { get; set; }
        public int Watch { get; set; }
        public DateTime CreateTime { get; set; }
    }
}