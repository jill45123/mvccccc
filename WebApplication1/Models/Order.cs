﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class Order
    {
        public string Order_Id { get; set; }
        public string Member_Id { get; set; }
        public int Total { get; set; }
        public string Receiver { get; set; }
        public string Phone { get; set; }
        public int Shipping_Id { get; set; }
        public int Payment_Id { get; set; }
        public string Address { get; set; }
        public DateTime OrderTime{ get; set; }
        public DateTime? ShippingTime { get; set; }
        public DateTime? PaymentTime { get; set; }
        public DateTime? DeliveryTime { get; set; }
        public int Status_Id { get; set; }
    }
}