﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class Status
    {
        public int Status_Id { get; set; }
        public string Status_Name { get; set; }
    }
}