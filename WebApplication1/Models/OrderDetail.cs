﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class OrderDetail
    {
        public int OrderDetail_Id { get; set; }
        public string Order_Id { get; set; }
        public string Item_Id { get; set; }
        public int Quantity { get; set; }
    }
}