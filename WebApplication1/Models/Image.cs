﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class Image
    {
        public int Image_Id { get; set; }
        public string FileName { get; set; }
        public string Url { get; set; }
        public int Type_Id { get; set; }
    }
}