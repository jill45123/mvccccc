﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class Reviews
    {
        public string Review_Id { get; set; }
        public string Member_Id { get; set; }
        public string Item_Id { get; set; }

        [Required(ErrorMessage = "請輸入評價內容")]
        public string Content { get; set; }

        [Required(ErrorMessage = "請輸入評價")]
        public string Ratings { get; set; }
        public DateTime CreateTime { get; set; }
    }
}