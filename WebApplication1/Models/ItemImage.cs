﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class ItemImage
    {
        public int ItemImage_Id { get; set; }
        public string Item_Id { get; set; }
        public int Image_Id { get; set; }
    }
}