﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class Cart
    {
        public int Cart_Id { get; set; }
        public string Member_Id { get; set; }
        public int Item_Id { get; set; }
        public int Quantity { get; set; }

    }
}